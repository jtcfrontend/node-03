var express = require('express');
var app = express();
var router = express.Router();
var fs = require('fs');
var path = require('path');
var jwt = require('../utils/jwt');
var authController = require('../controllers/auth.controller');

module.exports = function () {
    router.post('/register', authController.register);
    router.post('/login', authController.login);
    return router;
}
