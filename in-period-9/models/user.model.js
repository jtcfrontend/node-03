var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var userSchema = new Schema({
    name: {
        type: String
    },
    username: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    salt: {
        type: String,
     
    }
});
// chong trung username
userSchema.index({username: 1}, {unique: true});
var User = mongoose.model('user', userSchema);

module.exports = User;