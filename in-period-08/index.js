var express = require('express');
var app = express();
var fs = require("fs");
var errorHandler = require("./middle-ware/error-handler");
var routes = require("./routes/index.route");
var allowCrossDomain = function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type, authorization');
    next();
};

app.use( require('body-parser').urlencoded({ extended: false }));
app.use( require('body-parser').json());
app.use(express.static('public'));
app.use(allowCrossDomain);

routes(app);



app.use(errorHandler.errorHandler());

var server = app.listen(8081, function () {
    console.log("Ung dung Node.js dang lang nghe tai dia chi: ")
})